CAR <- function(data){
	s <- dim(data)[2]
	mat <- matrix(1, s, s)/(-s)
	diag(mat) <- 1
	return(data %*% mat)
}

RawData <- function(path){
	data <- as.matrix(read.csv(sprintf("%s/data.csv", path), skip=1, nrow=100000))*10000
	data <- data[,6:18]
	print(dim(data))
	colnames(data) <- NULL
	return(data)
}

Annotations <- function(path){
	ret <- read.csv(sprintf("%s/annotations.csv", path))
	names(ret) <- NULL
	return(ret)
}

Epochs <- function(data, l, gap){
	max_lenght = as.integer(dim(data)[1] - l)
	ret <- list()
	for (index in seq(1, max_lenght, gap)){
		int_down = as.integer(index)
		int_up = as.integer(index+l)
		ret[[as.integer(index/gap) + 1]] <- data[int_down:int_up, ]
	}
	return(ret)
}

BurgChannel <- function(data, order){
	s <- seq(1,length(data)) 
	for (i in s){s[i] = 1-((i-1-length(s)/2)/(length(s)/2))^2}
	data <- data*s

	bp <- b <- data[-length(data)]
	fp <- f <- data[-1]
	
	k <- list()
	for (i in seq(1,order+1)){
		divisor <- sum(b*b)+sum(f*f)
		if(divisor == 0){
			k[i] <- 0
		}else{
			k[i] <- -2.0*(sum(b*f)/divisor)
		}
		f <- (fp + k[[i]]*bp)[-1]
		b <- (bp + k[[i]]*fp)[-length(bp)]
		fp <- f
		bp <- b
	}
	dim(k) <- NULL
	return(rev(k))
}

BurgEpoch <- function(epoch){
	ret <- c()
	for (i in seq(1,length(epoch[1,]))){
		ret <- c(ret, BurgChannel(epoch[,i], 20))
	}
	return(ret)
}

ARModelsOf <- function(epochs){
	ret <- data.frame(BurgEpoch(epochs[[1]]))
	for (i in seq(2, length(epochs))){
		print(i)
		ret[i,] <- BurgEpoch(epochs[[i]])
		if ((i%%400) == 0){
			gc()
		}
	}
	return(ret)
}

PolyExpansionFrom <- function(data){
	return(poly(data, 2, raw=TRUE))
}


BIC <- function(mse, p, n){
	return(n*log(mse) + p*log(n))
}

library(e1071)
library(C50)

DecisionTree <- function(X1, Y1, X2, idx){
	dtm <- C5.0(X1[,idx], as.factor(Y1[,1]))
	return(predict(dtm, X2[,idx]))
}

KNN <- function(X1, Y1, X2, idx){
	return(knn(train=X1[,idx], test=X2[,idx], cl=c(Y1[,1]), k=5))
}

library(mltools)
ForwardSelection <- function(classifier, X, Y, X2){
	xshape <- dim(X)
	best_crit <- Inf 
	best_idx <- c()
	classes = unique(Y[,1])
	Yenc <- c(Y[,1])
	
	const <- 5
	Yenc[] <- const
	results_enc <- Yenc
	# channel length = 20 + 1 index offset
	ch_len <- 21
		
	for (i in seq(1, as.integer(xshape[2]/ch_len))){
		new_idx <- list()
		lowest_crit <- Inf	
	
		print(i)
		for (j in seq(1,as.integer(xshape[2]/ch_len))){
			test_idx <- unique(c(best_idx, (j-1)*ch_len + seq(1,ch_len)))
			results <- classifier(X, Y, X2, test_idx)
			gc()
			results_enc[] <-0
			results_enc[which(results==c(Y[,1]))] <- const
			test_crit <- BIC(mse(Yenc, results_enc), i+2, xshape[1])
			
			if(test_crit < lowest_crit){
				new_idx <- test_idx
				lowest_crit <- test_crit
			}
		}
		print(lowest_crit)
		if(lowest_crit >= best_crit){ break }
		
		best_crit <- lowest_crit
		best_idx <- new_idx
	}
	return(best_idx)	
}

library(gridExtra)
plt <- function(ctab, name, w, h){
	png(name, w, h)
	grid.table(ctab$t)
	dev.off()
}

library(class)
library(gmodels)
main <- function(){
	
	annotations <- Annotations("data")
	data <- CAR(RawData("data"))
	data <- poly(data,2,raw=TRUE)	
	f_vectors <<- ARModelsOf(Epochs(data,128,32))
	
	f_vectors <<- f_vectors[seq(1,3000),]
	ann <<- annotations[seq(1,3000),]
	
	x_train1 <<- f_vectors[seq(1,2500),]
	y_train1 <<- ann[seq(1,2500),]
	
	x_test1 <<- f_vectors[seq(2501,3000),]
	y_test1 <<- ann[seq(2501,3000),1]
	
	x_train2 <<- f_vectors[seq(1,3000,2),]
	y_train2 <<- ann[seq(1,3000,2),]
	
	x_test2 <<- f_vectors[-seq(1,3000,2),]
	y_test2 <<- ann[-seq(1,3000,2),1]

	knn_best_idx1 <<- ForwardSelection(KNN, x_train1, y_train1, x_test1)
	knn_best_idx2 <<- ForwardSelection(KNN, x_train2, y_train2, x_test2)
	
	knn_res1 <<- KNN(x_train1, y_train1, x_test1, knn_best_idx1)
	knn_res2 <<- KNN(x_train2, y_train2, x_test2, knn_best_idx2)

	dt_best_idx1 <<- ForwardSelection(DecisionTree, x_train1, y_train1, x_test1)
	dt_best_idx2 <<- ForwardSelection(DecisionTree, x_train2, y_train2, x_test2)
	
	dt_res1 <<- DecisionTree(x_train1, y_train1, x_test1, dt_best_idx1)
	dt_res2 <<- DecisionTree(x_train2, y_train2, x_test2, dt_best_idx2)

	knn1_tab <<- CrossTable(y_test1[,1], knn_res1, prop.c=FALSE, prop.r=FALSE, prop.t=FALSE, prop.chisq=FALSE)
	knn2_tab <<- CrossTable(y_test2[,1], knn_res2, prop.c=FALSE, prop.r=FALSE, prop.t=FALSE, prop.chisq=FALSE)

	dt1_tab <<- CrossTable(y_test1[,1], dt_res1, prop.c=FALSE, prop.r=FALSE, prop.t=FALSE, prop.chisq=FALSE)
	dt2_tab <<- CrossTable(y_test2[,1], dt_res2, prop.c=FALSE, prop.r=FALSE, prop.t=FALSE, prop.chisq=FALSE)
	
	plt(knn1_tab, "knn1.png", 150, 150)
	plt(knn2_tab, "knn2.png", 150, 150)

	plt(dt1_tab, "dt1.png", 150, 150)
	plt(dt2_tab, "dt2.png", 150, 150)
}
